
getConfig();
bindEvent();

function getConfig()
{
    console.log('getConfig() called...');

    chrome.storage.local.get(['user_config'], function(result) {
       if ((typeof result.user_config == 'undefined') || !result.user_config) {
           console.log('no data.');
           return;
       }
       console.log(result.user_config);
       document.getElementById('black_id_list').value = result.user_config;
    });
}

function bindEvent()
{
    console.log('bindEvent() called...');

    let button = document.getElementById('save_btn');
    let loading = document.getElementById('loading_btn');
    button.addEventListener('click', function() {
        console.log('save_btn on click...');
        let content = document.getElementById('black_id_list').value;
        button.style.display = 'none';
        loading.style.display="block";
        setTimeout(function(){
            chrome.storage.local.set({
                "user_config" : content,
            }, function(){
                console.log('saved.');
                loading.style.display="none";
                button.style.display = 'block';
            });
        }, 500);
    });
}