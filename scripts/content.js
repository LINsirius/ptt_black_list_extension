var userConfig = '';
var idArray = [];
var onPolling = false; 
var timer;

run();

function run()
{
    console.log('run() called...');

    chrome.storage.local.get(['user_config'], function(result) {
        if ((typeof result.user_config == 'undefined') || !result.user_config) {
            console.log('no user_config.');
            return;
        }
        userConfig = result.user_config;
        idArray = userConfig.split('\n');
        idArray.forEach(function(id){
            map(id);
        });
    });

    bindPollingEvent();
}

function map(targetId='')
{
    console.log('map() called...' + targetId);

    let divs = document.querySelectorAll("div.push");
    let pattern = new RegExp(targetId);

    divs.forEach(function(div){
        let el = div.querySelector('span.push-userid');
        if (el) {
            let id = el.textContent;
            if (id.match(pattern)) {
                console.log('killed');
                div.innerHTML = "";
            }
        }
    });
}

function bindPollingEvent()
{
    console.log('bindPollingEvent() called...');

    let btn = document.getElementById("article-polling");
    if (btn) {
        btn.addEventListener("click", function(e){
            onPolling = !onPolling;
            console.log(onPolling);
            if (onPolling) {
                timer = setInterval(function(){
                    let text = btn.className;
                    if (text && text.match(/fatal-error/)) {
                        console.log('exception error.');
                        clearInterval(timer);
                        return;
                    }
                    idArray.forEach(function(id){
                        map(id);
                    });
                }, 500);
            } else {
                clearInterval(timer);
            }
        });
    }
}